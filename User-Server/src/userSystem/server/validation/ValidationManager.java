package userSystem.server.validation;

import java.util.UUID;

import userSystem.data.User;
import userSystem.data.ValidatedUser;
import userSystem.logger.UserSystemLogger;

public class ValidationManager {

	private final ValidationKeyService vks = new ValidationKeyService();
	private final ValidationUserService vus = new ValidationUserService();

	public ValidatedUser login(String userIp, User user) {
		ValidatedUser oldUser = vus.getValidatedUser(userIp, user.getId());
		UserSystemLogger.debug("register login: (" + userIp + " , " + user + ") -> " + oldUser, user.getName());
		if (oldUser == null) {
			ValidatedUser newUser = generateValidatedUser(userIp, user);
			vus.putValidatedUser(newUser);
			UserSystemLogger.debug("New login: (" + userIp + " , " + user + ") -> " + newUser, user.getName());
			return newUser;
		} else if (oldUser.isValid()) {
			oldUser.update();
			UserSystemLogger.debug("Old login: (" + userIp + " , " + user + ") -> " + oldUser, user.getName());
			return oldUser;
		} else {
			ValidatedUser newUser = generateValidatedUser(userIp, user);
			vus.removeValidatedUser(userIp, user.getId());
			vus.putValidatedUser(newUser);
			UserSystemLogger.debug("ExpiredToNew login: (" + userIp + " , " + user + ") -> " + newUser, user.getName());
			return newUser;
		}
	}

	public ValidatedUser validate(String userIp, UUID userId, String validationKey) {
		ValidatedUser oldUser = vus.getValidatedUser(userIp, userId);
		UserSystemLogger.debug("validate user: (" + userIp + " , " + userId + ") -> " + oldUser, userId.toString());
		if (oldUser == null) {
			UserSystemLogger.debug("validation failed");
			return null;
		} else if (oldUser.isValid()) {
			if (!oldUser.getValidationKey().equals(validationKey)) {
				UserSystemLogger.debug("validation failed");
				return null;
			}
			oldUser.update();
			UserSystemLogger.debug("validation succeeded");
			return oldUser;
		} else {
			vus.removeValidatedUser(userIp, userId);
			UserSystemLogger.debug("validationKey expired");
			return null;
		}
	}

	public void logout(String userIp, UUID userId) {
		ValidatedUser oldUser = vus.getValidatedUser(userIp, userId);
		if (oldUser == null) {
			return;
		} else if (oldUser.isValid()) {
			oldUser.expire();
			vus.removeValidatedUser(userIp, userId);	
		} else {
			vus.removeValidatedUser(userIp, userId);
		}
	}

	private ValidatedUser generateValidatedUser(String userIp, User user) {
		return new ValidatedUser(user, vks.generateValidationKey(), userIp);
	}

	public ValidatedUser currentUser(String userIp, UUID userID) {
		return vus.getValidatedUser(userIp, userID);
	}

}
