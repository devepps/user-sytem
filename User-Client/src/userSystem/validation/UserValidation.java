package userSystem.validation;

import java.util.UUID;

import baseSC.data.internelDTO.SubscribedMessageDTO;
import baseSC.data.subscriber.SubscribedStatus;
import userSystem.data.User;
import userSystem.data.ValidatedUser;
import userSystem.data.dataObjects.UserDO;
import userSystem.data.dataObjects.ValidatedUserDO;
import userSystem.logger.UserSystemLogger;
import userSystem.login.UserLogin;

public class UserValidation extends UserLogin {
	
	public UserValidation() {
		super();
	}
	
	public ValidatedUser validateUser(UUID userId, String validationKey, String ip) {
		SubscribedMessageDTO loginAnswer = clientManager.subscribe(new ValidatedUserDO(userId, validationKey, ip), REQUEST_VALID_DURATION);
		UserSystemLogger.debug("Validation-Request: <" + userId + "> <" + validationKey + "> -> " + loginAnswer);
		if (loginAnswer.getSmHeader().getStatusId() == SubscribedStatus.OK.getId()) {
			UserDO user = (UserDO) loginAnswer.getContent();
			return new ValidatedUser(new User(userId, user.getName(), User.ANONYMOUS_PASSWORD,
					User.ANONYMOUS_PASSWORD), validationKey, ip);
		} else {
			return null;
		}
	}

}
