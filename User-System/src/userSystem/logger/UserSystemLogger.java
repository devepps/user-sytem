package userSystem.logger;

import config.ConfigEnvironment;
import logger.main.Logger;
import logger.main.prefixs.TimePrefix;

public class UserSystemLogger extends Logger {

	private static final boolean debug = Boolean.parseBoolean(ConfigEnvironment.getProperty("Status.UserSystem.Debug"));

	private static final UserSystemLogger logger = new UserSystemLogger();

	public UserSystemLogger() {
		super("UserSystem", new TimePrefix("dd.MM.yy"), new TimePrefix("hh:mm:ss"));
	}
	
	public static void debug(String msg) {
		if(!debug) return;
		logger.println("[debug] " + msg);
	}

	public static void debug(String msg, long clientId) {
		if(!debug) return;
		logger.println("[debug] " + "[" + clientId + "] " + msg);
	}

	public static void debug(String msg, String userName) {
		if(!debug) return;
		logger.println("[debug] " + "[" + userName + "] " + msg);
	}
	
	public static void info(String msg) {
		logger.println("[info] " + msg);
	}

	public static void info(String msg, long clientId) {
		logger.println("[info] " + "[" + clientId + "] " + msg);
	}

	public static void info(String msg, String userName) {
		logger.println("[info] " + "[" + userName + "] " + msg);
	}
}
