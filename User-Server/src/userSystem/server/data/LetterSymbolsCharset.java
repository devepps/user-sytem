package userSystem.server.data;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CoderResult;
import java.nio.charset.StandardCharsets;

public class LetterSymbolsCharset extends Charset {

	public static final Charset OWN = new LetterSymbolsCharset();

	private static final Charset CHARSET = StandardCharsets.ISO_8859_1;
	private final String content = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabzdefghijklmnopqrstuvwxyz,;.:-_#+<>[]{}()";

	public LetterSymbolsCharset() {
		super("LetterSymbols", new String[] { "LetterSymbols" });
	}

	@Override
	public boolean contains(Charset cs) {
		return cs.aliases().contains(CHARSET.name()) || cs.contains(CHARSET);
	}

	@Override
	public CharsetDecoder newDecoder() {
		return new CharsetDecoder(this, 1f, 1f) {
			
			@Override
			protected CoderResult decodeLoop(ByteBuffer in, CharBuffer out) {
				while (in.hasRemaining()) {
					int id = in.get();
					while(id >= content.length()) id -= content.length();
					while(id < 0) id += content.length();
					out.append(content.charAt(id));
				}
				return CoderResult.UNDERFLOW;
			}
		};
	}

	@Override
	public CharsetEncoder newEncoder() {
		return CHARSET.newEncoder();
	}

}
