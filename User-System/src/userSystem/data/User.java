package userSystem.data;

import java.util.UUID;

public class User{

	public static final User DEFAULT_SYSTEM_USER = new User(null, "Default_System_User", "{Default_System_User(Passwort)}", "defaul_user@system.de");
	public static final String ANONYMOUS_PASSWORD = "******";
	public static final String ANONYMOUS_EMAIL = "******";

	private UUID id;
	private String name = "";
	private String password = "";
	private String eMail = "";

	public User() {
		super();
	}

	public User(UUID id, String name, String password, String eMail) {
		super();
		this.id = id;
		this.name = name;
		this.password = password;
		this.eMail = eMail;
	}

	public UUID getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getPassword() {
		return password;
	}

	public String getEMail() {
		return eMail;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", password=" + password + ", eMail=" + eMail + "]";
	}
}
