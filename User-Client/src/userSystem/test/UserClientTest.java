package userSystem.test;

import java.util.UUID;

import userSystem.creation.UserCreation;
import userSystem.data.User;
import userSystem.data.ValidatedUser;
import userSystem.data.dataObjects.UserDO;
import userSystem.logger.UserSystemLogger;
import userSystem.validation.UserValidation;

public class UserClientTest {

	public static void main(String[] args) {
		new UserClientTest();
	}

	public UserClientTest() {
		UserValidation userValidation = new UserValidation();
		UserCreation userCreation = new UserCreation();

		ValidatedUser validatedUser = userCreation.login(User.DEFAULT_SYSTEM_USER.getName(), User.DEFAULT_SYSTEM_USER.getPassword());
		UserSystemLogger.info("Default user " + User.DEFAULT_SYSTEM_USER + " has been logged in : " + validatedUser);
		UserSystemLogger.info("Default user " + User.DEFAULT_SYSTEM_USER + " has been logged in : " + userCreation.login(User.DEFAULT_SYSTEM_USER.getName(), User.DEFAULT_SYSTEM_USER.getPassword()));

		UserDO newUser = new UserDO(UUID.randomUUID(), "Test1", "Password1", "test@test.de");
		UserSystemLogger.info("User has been created: " + userCreation.createUser(newUser.getName(), newUser.getPassword(), newUser.getEMail()));
		UserSystemLogger.info("New user " + newUser + " has been logged in : " + userCreation.login(newUser.getName(), newUser.getPassword())); 

		UserSystemLogger.info("User-Validation \"" + validatedUser + "\" has resultet in : " + userValidation.validateUser(validatedUser.getUser().getId(), validatedUser.getValidationKey(), "127.0.0.1"));

		ValidatedUser validatedUser2 = userValidation.login(newUser.getName(), newUser.getPassword());
		UserSystemLogger.info("New user " + newUser + " has been logged in : " + validatedUser2);
		UserSystemLogger.info("New user " + newUser + " has been logged in : " + userCreation.login(newUser.getName(), newUser.getPassword()));

		UserSystemLogger.info("User-Validation \"" + validatedUser + "\" has resultet in : " + userValidation.validateUser(validatedUser.getUser().getId(), validatedUser.getValidationKey(), "127.0.0.1"));		
		UserSystemLogger.info("User-Validation \"" + validatedUser + "\" has resultet in : " + userValidation.validateUser(validatedUser.getUser().getId(), validatedUser.getValidationKey(), "127.0.0.1"));		
		UserSystemLogger.info("User-Validation \"" + validatedUser2 + "\" has resultet in : " + userValidation.validateUser(validatedUser2.getUser().getId(), validatedUser2.getValidationKey(), "127.0.0.1"));
	}

}
