package userSystem.server.data;

import java.util.UUID;

import collection.sync.SyncArrayList;
import userSystem.data.ValidatedUser;
import userSystem.logger.UserSystemLogger;

public class ValidationInformation {

	private final SyncArrayList<ValidatedUser> validatedUsers = new SyncArrayList<>();

	public ValidatedUser get(UUID userId) {
		for (int i = 0; i < validatedUsers.size(); i++) {
			if (userId.equals(validatedUsers.get(i).getUser().getId())) {
				return validatedUsers.get(i);
			}
		}
		return null;
	}

	public void addValidatedUser(ValidatedUser user) {
		validatedUsers.add(user);
	}

	public void remove(UUID userId) {
		for (int i = 0; i < validatedUsers.size(); i++) {
			if (userId.equals(validatedUsers.get(i).getUser().getId())) {
				UserSystemLogger.debug("Removed validation: " + validatedUsers.remove(i), userId.toString());
				return;
			}
		}
	}

}
