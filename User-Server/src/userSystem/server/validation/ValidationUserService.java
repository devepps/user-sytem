package userSystem.server.validation;

import java.util.UUID;

import collection.sync.SyncHashMap;
import userSystem.data.ValidatedUser;
import userSystem.server.data.ValidationInformation;

public class ValidationUserService {

	private final SyncHashMap<String, ValidationInformation> validatedUsers = new SyncHashMap<>();
	
	public ValidatedUser getValidatedUser(String userIp, UUID userId) {
		ValidationInformation validationInformation = this.validatedUsers.get(userIp);
		if (validationInformation == null) {
			return null;
		}
		return validationInformation.get(userId);
	}
	
	public void putValidatedUser(ValidatedUser user) {
		ValidationInformation validationInformation = this.validatedUsers.get(user.getIP());
		if (validationInformation == null) {
			validationInformation = new ValidationInformation();
			this.validatedUsers.put(user.getIP(), validationInformation);
		}
		validationInformation.addValidatedUser(user);
	}
	
	public void removeValidatedUser(String userIp, UUID userId) {
		ValidationInformation validationInformation = this.validatedUsers.get(userIp);
		if (validationInformation != null) {
			validationInformation.remove(userId);
		}
	}

}
