package userSystem.server;

import java.util.ArrayList;

import baseSC.server.ServerManager;
import collection.tick.TickManager;
import config.ConfigEnvironment;
import repositoryClient.client.RepoClientInterface;
import repositoryClient.data.Repository;
import userSystem.data.User;
import userSystem.data.dataObjects.LoginDO;
import userSystem.data.dataObjects.PuppetLoginDO;
import userSystem.data.dataObjects.UserCreationDO;
import userSystem.data.dataObjects.UserDO;
import userSystem.data.dataObjects.ValidatedUserDO;
import userSystem.logger.UserSystemLogger;

public class UserServer {

	public static void main(String[] args) {
		new UserServer();
	}

	private ServerManager serverManager;
	private RepoClientInterface repoClient;
	private Repository<UserDO> userRepo;

	public UserServer() {
		this.serverManager = new ServerManager(Integer.parseInt(ConfigEnvironment.getProperty("User.Server.Port")),
				"User.Server.SSL.KeyStorePath", "User.Server.SSL.KeyStorePassword");
		this.repoClient = new RepoClientInterface("Repository.Client.Connection.User-PW.Ip", "Repository.Client.Connection.User-PW.Port", "Repository.Client.Connection.User-PW.keyStorePath");
		this.userRepo = repoClient.createRepository(UserDO.class);

		serverManager.getEventManager().enableParallelMode();
		serverManager.openConnection();
		new TickManager(() -> serverManager.getEventManager().tick(), 1);
		new TickManager(() -> repoClient.tick(), 1);

		serverManager.getDataPackageManager().createPackageType(LoginDO.class);
		serverManager.getDataPackageManager().createPackageType(UserCreationDO.class);
		serverManager.getDataPackageManager().createPackageType(UserDO.class);
		serverManager.getDataPackageManager().createPackageType(ValidatedUserDO.class);
		serverManager.getDataPackageManager().createPackageType(PuppetLoginDO.class);
		UserServerEM userServerEM = new UserServerEM(this);
		
		serverManager.getEventManager().registerNewClientConnectionEventListener(userServerEM, 1);
		serverManager.getEventManager().registerServerLostConnectionToClientEventListener(userServerEM, 1);
		serverManager.getEventManager().registerServerMessageEventListener(userServerEM, 1);
		
		ArrayList<UserDO> defaultUser = userRepo.getByData(User.DEFAULT_SYSTEM_USER.getName(), "name");
		if (defaultUser == null || defaultUser.size() != 1 || defaultUser.get(0) == null
				|| !defaultUser.get(0).getEMail().equals(User.DEFAULT_SYSTEM_USER.getEMail())
				|| !defaultUser.get(0).getPassword().equals(User.DEFAULT_SYSTEM_USER.getPassword())) {
			UserSystemLogger.info(
					"Default system user \"" + userRepo.add(new UserDO(User.DEFAULT_SYSTEM_USER)) + "\" has been added.",
					User.DEFAULT_SYSTEM_USER.getName());
		} else {
			UserSystemLogger.info(
					"Default system user \"" + defaultUser.get(0) + "\" has been found.",
					User.DEFAULT_SYSTEM_USER.getName());
		}
	}

	public User login(String username, String password) {
		ArrayList<UserDO> users = userRepo.getByData(username, "name");
		if (users == null || users.size() != 1) {
			UserSystemLogger.debug("has failed a Login. Reason: Unknown username", username);
			return null;
		} else {
			UserDO user = users.get(0);
			if (user == null || !user.getPassword().equals(password)) {
				UserSystemLogger.debug("has failed a Login. Reason: Invalid password", username);
				return null;
			}
			return new User(user.getId(), user.getName(), User.ANONYMOUS_PASSWORD, user.getEMail());
		}
	}

	public boolean createUser(String name, String password, String eMail) {
		UserDO userDTO = new UserDO(null, name, password, eMail);
		userDTO = this.userRepo.add(userDTO);
		return userDTO == null ? false : true;
	}

	public String getIp(long clientId) {
		String ip = serverManager.getClientIP(clientId);
		UserSystemLogger.debug("IP \"" + ip + "\" found.", clientId);
		return ip;
	}
}
