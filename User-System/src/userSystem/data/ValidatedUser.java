package userSystem.data;

import static userSystem.data.User.ANONYMOUS_PASSWORD;

import config.ConfigEnvironment;

public class ValidatedUser {
	
	private static final int MAX_VALID_DURATION = ConfigEnvironment.getPropertyInt("UserSystem.validation.key.max_duration");
	
	private User user;
	private long loggedin;
	private String validationKey;
	private String ip;

	public ValidatedUser(User user, String validationKey, String ip) {
		this.user = new User(user.getId(), user.getName(), ANONYMOUS_PASSWORD, user.getEMail());
		this.loggedin = System.currentTimeMillis();
		this.validationKey = validationKey;
		this.ip = ip.toLowerCase();
	}

	public User getUser() {
		return user;
	}

	public String getValidationKey() {
		return validationKey;
	}

	public String getIP() {
		return ip;
	}

	public void update() {
		this.loggedin = System.currentTimeMillis();
	}

	public boolean isValid() {
		return System.currentTimeMillis() - loggedin < MAX_VALID_DURATION;
	}

	public void expire() {
		this.loggedin = 0;
	}

	@Override
	public String toString() {
		return "ValidatedUser [user=" + user + ", loggedIn=" + loggedin + ", validationKey=" + "****"
				+ ", ip=" + ip + "]";
	}

}
