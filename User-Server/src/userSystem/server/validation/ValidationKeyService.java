package userSystem.server.validation;

import java.util.Random;

import userSystem.server.data.LetterSymbolsCharset;

public class ValidationKeyService {

	private Random random;

	public ValidationKeyService() {
		this.random = new Random();
	}

	public String generateValidationKey() {
		byte[] data = new byte[512];
		random.nextBytes(data);
		return new String(data, LetterSymbolsCharset.OWN);
	}

}
