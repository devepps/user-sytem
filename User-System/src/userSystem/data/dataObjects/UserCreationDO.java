package userSystem.data.dataObjects;

import java.io.Serializable;

import baseSC.data.DTO;
import baseSC.data.packageType.PackagableContent;
import baseSC.data.packageType.PackagableEntity;
import baseSC.data.types.DefaultValue;
import baseSC.data.types.SingleDataReaderType;

@PackagableEntity(configKey = "Connection.DTO-Ids.UserSystem.UserCreation")
public class UserCreationDO implements DTO, Serializable {

	private static final long serialVersionUID = 365099233617162107L;

	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_STRING_UNICODE, defaultValueId = DefaultValue.ID_STRING_EMPTY)
	private String name = "";

	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_STRING_UNICODE, defaultValueId = DefaultValue.ID_STRING_EMPTY)
	private String password = "";

	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_STRING_UNICODE, defaultValueId = DefaultValue.ID_STRING_EMPTY)
	private String eMail = "";

	public UserCreationDO() {
		super();
	}

	public UserCreationDO(String name, String password, String eMail) {
		super();
		this.name = name;
		this.password = password;
		this.eMail = eMail;
	}

	public String getName() {
		return name;
	}

	public String getPassword() {
		return password;
	}

	public String getEMail() {
		return eMail;
	}

	@Override
	public DTO copy() {
		return new UserCreationDO(name, password, eMail);
	}

	@Override
	public String toString() {
		return "LoginDO [name=" + name + ", password= *****, eMail= " + eMail + "]";
	}
}
