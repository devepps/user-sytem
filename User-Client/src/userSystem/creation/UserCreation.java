package userSystem.creation;

import baseSC.data.internelDTO.SubscribedMessageDTO;
import baseSC.data.subscriber.SubscribedStatus;
import userSystem.data.dataObjects.UserCreationDO;
import userSystem.logger.UserSystemLogger;
import userSystem.login.UserLogin;

public class UserCreation extends UserLogin {

	public UserCreation() {
		super();
	}

	public boolean createUser(String username, String password, String eMail) {
		SubscribedMessageDTO userCreationAnswer = clientManager.subscribe(new UserCreationDO(username, password, eMail), REQUEST_VALID_DURATION);
		UserSystemLogger.debug("Creation-Request: <" + username + "> <" + eMail + "> -> " + userCreationAnswer);
		return userCreationAnswer.getSmHeader().getStatusId() == SubscribedStatus.OK.getId();
	}

}
