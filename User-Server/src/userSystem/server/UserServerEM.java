package userSystem.server;

import baseSC.data.events.server.NewClientConnectionEvent;
import baseSC.data.events.server.NewClientConnectionEventListener;
import baseSC.data.events.server.ServerEvent;
import baseSC.data.events.server.ServerLostConnectionToClientEvent;
import baseSC.data.events.server.ServerLostConnectionToClientEventListener;
import baseSC.data.events.server.SubscribedMessageEvent;
import baseSC.data.events.server.ToServerMessageEvent;
import baseSC.data.events.server.ToServerMessageEventListener;
import baseSC.data.internelDTO.SubscribedMessageDTO;
import baseSC.data.subscriber.SubscribedStatus;
import collection.sync.SyncHashMap;
import userSystem.data.User;
import userSystem.data.ValidatedUser;
import userSystem.data.dataObjects.LoginDO;
import userSystem.data.dataObjects.PuppetLoginDO;
import userSystem.data.dataObjects.UserCreationDO;
import userSystem.data.dataObjects.UserDO;
import userSystem.data.dataObjects.ValidatedUserDO;
import userSystem.logger.UserSystemLogger;
import userSystem.server.validation.ValidationManager;

public class UserServerEM implements ToServerMessageEventListener, NewClientConnectionEventListener,
		ServerLostConnectionToClientEventListener {

	private UserServer userServer;
	
	private final ValidationManager validationManager = new ValidationManager();	
	private final SyncHashMap<Long, ValidatedUser> validatedUsers = new SyncHashMap<>();

	public UserServerEM(UserServer userServer) {
		super();
		this.userServer = userServer;
	}

	@Override
	public void connectionLost(ServerLostConnectionToClientEvent event) {
		ValidatedUser user = validatedUsers.get(event.getClientID());
		if (user != null) {
			validationManager.logout(user.getIP(), user.getUser().getId());
			validatedUsers.remove(event.getClientID());
		}
		log("has logged out at " + event.getCreatedTime(), event, user);
		event.setActive(false);
	}

	@Override
	public void newServerClient(NewClientConnectionEvent event) {
		UserSystemLogger.info("has logged in at " + event.getCreatedTime(), event.getClientID());
		event.setActive(false);
	}

	@Override
	public void messageFromClient(ToServerMessageEvent event) {
		event.setActive(false);
		log("has made an invalid request : " + event, event);
	}

	@Override
	public void messageFromClient(SubscribedMessageEvent event) {
		ValidatedUser currentUser = validatedUsers.get(event.getClientID());
		try {
			if (event.getRequest().getContent() == null) {
				log("has made an invalid request : " + event, event, currentUser);
			} else if (event.getRequest().getContent() instanceof LoginDO) {
				handleLoginRequest(event, currentUser);
			} else if (event.getRequest().getContent() instanceof PuppetLoginDO) {
				handlePuppetLoginRequest(event, currentUser);
			} else if (event.getRequest().getContent() instanceof ValidatedUserDO) {
				handleValidationRequest(event, currentUser);
			} else if (event.getRequest().getContent() instanceof UserCreationDO) {
				handleUserCreationRequest(event, currentUser);				
			} else {
				log("has made an invalid request : " + event, event, currentUser);				
			}
		} catch (Throwable e) {
			log("An error occured while handling the request : \"" + event + "\"  ->  " + e.getMessage(), event, currentUser);		
			event.getRequest().getSmHeader().setStatus(SubscribedStatus.FAILURE.getId());
			e.printStackTrace();
		}
		event.setActive(false);
		log("Request hast been answered with: " + event, event, currentUser);
	}

	private void handleUserCreationRequest(SubscribedMessageEvent event, ValidatedUser currentUser) {
		if (currentUser == null) {
			log("has made a usercreation request : " + event + ", but was not logged in", event, currentUser);
			event.getRequest().getSmHeader().setStatus(SubscribedStatus.BAD_REQUEST.getId());
		} else {
			log("has made a usercreation request : " + event, event, currentUser);
			UserCreationDO userCreationRequest = ((UserCreationDO) event.getRequest().getContent());
			boolean success = userServer.createUser(userCreationRequest.getName(), userCreationRequest.getPassword(), userCreationRequest.getEMail());
			if (!success) {
				log("usercreation request has failed" + event, event, currentUser);
				event.getRequest().getSmHeader().setStatus(SubscribedStatus.BAD_REQUEST.getId());
			} else {
				log("validausercreationtion request has succeded" + event, event, currentUser);
				event.getRequest().getSmHeader().setStatus(SubscribedStatus.OK.getId());
				return;
			}
		}
		event.getRequest().getSmHeader().setStatus(SubscribedStatus.BAD_REQUEST.getId());
	}

	private void handleValidationRequest(SubscribedMessageEvent event, ValidatedUser currentUser) {
		if (currentUser == null) {
			log("has made a validation request : " + event + ", but was not logged in", event, currentUser);
			event.getRequest().getSmHeader().setStatus(SubscribedStatus.BAD_REQUEST.getId());
		} else {
			log("has made a validation request : " + event, event, currentUser);
			ValidatedUserDO vur = ((ValidatedUserDO) event.getRequest().getContent());
			ValidatedUser foundValidatedUser = validationManager.validate(vur.getIp(), vur.getId(), vur.getValidationKey());
			if (foundValidatedUser == null) {
				log("validation request has failed" + event, event, currentUser);
				event.getRequest().getSmHeader().setStatus(SubscribedStatus.BAD_REQUEST.getId());
			} else {
				log("validation request has succeded" + event, event, currentUser);
				event.setAnswer(new SubscribedMessageDTO(event.getRequest().getSmHeader(), new UserDO(foundValidatedUser.getUser())));
				event.getRequest().getSmHeader().setStatus(SubscribedStatus.OK.getId());
				return;
			}
		}
		event.getRequest().getSmHeader().setStatus(SubscribedStatus.BAD_REQUEST.getId());
	}

	private void handleLoginRequest(SubscribedMessageEvent event, ValidatedUser currentUser) {
		if (currentUser != null) {
			log("has made a login request : " + event + ", but was already logged in", event, currentUser);
		} else {
			log("has made a login request : " + event, event, currentUser);
			LoginDO puppetLoginDTO = (LoginDO) event.getRequest().getContent();
			User user = userServer.login(puppetLoginDTO.getName(), puppetLoginDTO.getPassword());
			if (user != null) {
				ValidatedUser validatedUser = validationManager.login(userServer.getIp(event.getClientID()), user);
				this.validatedUsers.put(event.getClientID(), validatedUser);
				log("has successfully a logged in : " + user + " -> " + validatedUser, event, validatedUser);
				event.setAnswer(new SubscribedMessageDTO(event.getRequest().getSmHeader(), new ValidatedUserDO(validatedUser)));
				event.getRequest().getSmHeader().setStatus(SubscribedStatus.OK.getId());
				return;
			} else {
				log("has failed a login : " + event, event, currentUser);
			}
		}
		event.getRequest().getSmHeader().setStatus(SubscribedStatus.BAD_REQUEST.getId());
	}

	private void handlePuppetLoginRequest(SubscribedMessageEvent event, ValidatedUser currentUser) {
		if (currentUser == null) {
			log("has made a puppet login request : " + event + ", but was not logged in", event);
		} else if (!currentUser.getUser().getName().equals(User.DEFAULT_SYSTEM_USER.getName())) {
			log("has made a puppet login request : " + event + ", but has no permission", event, currentUser);
		} else {
			log("has made a puppet login request : " + event, event, currentUser);
			PuppetLoginDO puppetLoginDTO = (PuppetLoginDO) event.getRequest().getContent();
			User user = userServer.login(puppetLoginDTO.getName(), puppetLoginDTO.getPassword());
			if (user != null) {
				ValidatedUser validatedUser = validationManager.login(puppetLoginDTO.getIP(), user);
				log("has successfully a puppet logged in : " + user + " -> " + validatedUser, event, validatedUser);
				event.setAnswer(new SubscribedMessageDTO(event.getRequest().getSmHeader(), new ValidatedUserDO(validatedUser)));
				event.getRequest().getSmHeader().setStatus(SubscribedStatus.OK.getId());
				return;
			} else {
				log("has failed a puppet login : " + event, event, currentUser);
			}
		}
		event.getRequest().getSmHeader().setStatus(SubscribedStatus.BAD_REQUEST.getId());
	}

	private void log(String msg, ServerEvent event) {
		ValidatedUser user = this.validatedUsers.get(event.getClientID());
		log(msg, event, user);
	}

	private void log(String msg, ServerEvent event, ValidatedUser user) {
		if (user == null) {
			UserSystemLogger.debug(msg, event.getClientID());
		} else {
			UserSystemLogger.debug(msg, user.getUser().getName());
		}
	}


}
