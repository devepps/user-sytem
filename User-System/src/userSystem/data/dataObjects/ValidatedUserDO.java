package userSystem.data.dataObjects;

import java.util.UUID;

import baseSC.data.DTO;
import baseSC.data.packageType.PackagableContent;
import baseSC.data.packageType.PackagableEntity;
import baseSC.data.types.DefaultValue;
import baseSC.data.types.SingleDataReaderType;
import userSystem.data.ValidatedUser;

@PackagableEntity(configKey = "Connection.DTO-Ids.UserSystem.ValidatedUserDTO")
public class ValidatedUserDO implements DTO {

	@PackagableContent(contentTypeKey = SingleDataReaderType.Key_UUID, defaultValueId = DefaultValue.ID_NULL)
	private UUID id;
	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_STRING_ISO_8859_1, defaultValueId = DefaultValue.ID_STRING_EMPTY)
	private String validationKey = "";
	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_STRING_ISO_8859_1, defaultValueId = DefaultValue.ID_STRING_EMPTY)
	private String ip;

	public ValidatedUserDO() {
		super();
	}

	public ValidatedUserDO(UUID id, String validationKey, String ip) {
		super();
		this.id = id;
		this.validationKey = validationKey;
		this.ip = ip.toLowerCase();
	}

	public ValidatedUserDO(ValidatedUser validatedUser) {
		this.id = validatedUser.getUser().getId();
		this.validationKey = validatedUser.getValidationKey();
		this.ip = validatedUser.getIP();
	}

	public UUID getId() {
		return id;
	}

	public String getValidationKey() {
		return validationKey;
	}

	public String getIp() {
		return ip;
	}

	@Override
	public String toString() {
		return "ValidatedUserDO [id=" + id + ", validationKey=" + validationKey + ", ip=" + ip + "]";
	}

	@Override
	public DTO copy() {
		return new ValidatedUserDO(id, validationKey, ip);
	}

}
