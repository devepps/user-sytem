package userSystem.data.dataObjects;

import java.io.Serializable;

import baseSC.data.DTO;
import baseSC.data.packageType.PackagableContent;
import baseSC.data.packageType.PackagableEntity;
import baseSC.data.types.DefaultValue;
import baseSC.data.types.SingleDataReaderType;

@PackagableEntity(configKey = "Connection.DTO-Ids.UserSystem.Login")
public class LoginDO implements DTO, Serializable {

	private static final long serialVersionUID = 365099233617162107L;

	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_STRING_UNICODE, defaultValueId = DefaultValue.ID_STRING_EMPTY)
	private String name = "";

	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_STRING_UNICODE, defaultValueId = DefaultValue.ID_STRING_EMPTY)
	private String password = "";

	public LoginDO() {
		super();
	}

	public LoginDO(String name, String password) {
		super();
		this.name = name;
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public String getPassword() {
		return password;
	}

	@Override
	public DTO copy() {
		return new LoginDO(name, password);
	}

	@Override
	public String toString() {
		return "LoginDO [name=" + name + ", password= ***** ]";
	}
}
