package userSystem.login;

import baseSC.client.ClientManager;
import baseSC.data.events.client.ClientLostConnectionToServerEvent;
import baseSC.data.events.client.ClientLostConnectionToServerEventListener;
import baseSC.data.internelDTO.SubscribedMessageDTO;
import baseSC.data.subscriber.SubscribedStatus;
import collection.tick.TickManager;
import config.ConfigEnvironment;
import userSystem.data.User;
import userSystem.data.ValidatedUser;
import userSystem.data.dataObjects.LoginDO;
import userSystem.data.dataObjects.PuppetLoginDO;
import userSystem.data.dataObjects.UserCreationDO;
import userSystem.data.dataObjects.UserDO;
import userSystem.data.dataObjects.ValidatedUserDO;
import userSystem.logger.UserSystemLogger;

public class UserLogin implements ClientLostConnectionToServerEventListener {
	
	protected static final long REQUEST_VALID_DURATION = Integer.parseInt(ConfigEnvironment.getProperty("Connection.maxMessageDuration"));

	protected ClientManager clientManager;

	public UserLogin() {
		this.clientManager = new ClientManager(ConfigEnvironment.getProperty("User.Server.IP"),
				Integer.parseInt(ConfigEnvironment.getProperty("User.Server.Port")), "User.Server.SSL.KeyStorePath");
		new TickManager(() -> clientManager.getEventManager().tick(), 1);

		clientManager.connectToServer();
		clientManager.getPackageManager().createPackageType(LoginDO.class);
		clientManager.getPackageManager().createPackageType(UserCreationDO.class);
		clientManager.getPackageManager().createPackageType(UserDO.class);
		clientManager.getPackageManager().createPackageType(ValidatedUserDO.class);
		clientManager.getPackageManager().createPackageType(PuppetLoginDO.class);

		clientManager.getEventManager().registerClientLostConnectionToServerEventListener(this, 1);
	}

	@Override
	public void connectionLost(ClientLostConnectionToServerEvent event) {
		UserSystemLogger.info("Lost connection to Server.");
		event.setActive(false);
	}

	public ValidatedUser login(String username, String password) {
		SubscribedMessageDTO loginAnswer = clientManager.subscribe(new LoginDO(username, password), REQUEST_VALID_DURATION);
		UserSystemLogger.debug("Login-Request: " + username + " -> " + loginAnswer);
		if (loginAnswer.getSmHeader().getStatusId() == SubscribedStatus.OK.getId()) {
			ValidatedUserDO validatedUser = (ValidatedUserDO) loginAnswer.getContent();
			return new ValidatedUser(new User(validatedUser.getId(), username, User.ANONYMOUS_PASSWORD,
					User.ANONYMOUS_EMAIL), validatedUser.getValidationKey(), "******");
		} else {
			return null;
		}
	}

	public ValidatedUser puppetLogin(String username, String password, String ip) {
		SubscribedMessageDTO loginAnswer = clientManager.subscribe(new PuppetLoginDO(username, password, ip), REQUEST_VALID_DURATION);
		UserSystemLogger.debug("Puppet-Login-Request: " + new PuppetLoginDO(username, password, ip) + " -> " + loginAnswer);
		if (loginAnswer.getSmHeader().getStatusId() == SubscribedStatus.OK.getId()) {
			ValidatedUserDO validatedUser = (ValidatedUserDO) loginAnswer.getContent();
			return new ValidatedUser(new User(validatedUser.getId(), username, User.ANONYMOUS_PASSWORD,
					User.ANONYMOUS_EMAIL), validatedUser.getValidationKey(), "******");
		} else {
			return null;
		}
	}

}
