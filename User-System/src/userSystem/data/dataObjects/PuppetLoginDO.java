package userSystem.data.dataObjects;

import java.io.Serializable;

import baseSC.data.DTO;
import baseSC.data.packageType.PackagableContent;
import baseSC.data.packageType.PackagableEntity;
import baseSC.data.types.DefaultValue;
import baseSC.data.types.SingleDataReaderType;

@PackagableEntity(configKey = "Connection.DTO-Ids.UserSystem.PuppetLogin")
public class PuppetLoginDO implements DTO, Serializable {

	private static final long serialVersionUID = 365099233617162107L;

	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_STRING_UNICODE, defaultValueId = DefaultValue.ID_STRING_EMPTY)
	private String name = "";

	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_STRING_UNICODE, defaultValueId = DefaultValue.ID_STRING_EMPTY)
	private String password = "";

	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_STRING_UNICODE, defaultValueId = DefaultValue.ID_STRING_EMPTY)
	private String ip = "";

	public PuppetLoginDO() {
		super();
	}

	public PuppetLoginDO(String name, String password, String ip) {
		super();
		this.name = name;
		this.password = password;
		this.ip = ip.toLowerCase();
	}

	public String getName() {
		return name;
	}

	public String getPassword() {
		return password;
	}

	public String getIP() {
		return ip;
	}

	@Override
	public DTO copy() {
		return new PuppetLoginDO(name, password, ip);
	}

	@Override
	public String toString() {
		return "PuppetLoginDO [name=" + name + ", ip=" + ip + ", password= ***** ]";
	}
}
